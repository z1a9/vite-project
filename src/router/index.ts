import { createRouter, createWebHistory } from 'vue-router';
import type { RouteRecordRaw } from 'vue-router';
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: 'index',
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('../views/index/index.vue'),
    children: [
      {
        path: '/index/home',
        name: 'home',
        component: () => import('../views/home/home.vue'),
      },
      {
        path: '/index/classify',
        name: 'classify',
        component: () => import('../views/Product/classify/classify.vue'),
      },
      {
        path: '/index/comments',
        name: 'comments',
        component: () => import('../views/Product/comments/comments.vue'),
      },
      {
        path: '/index/grouping',
        name: 'grouping',
        component: () => import('../views/Product/grouping/grouping.vue'),
      },
      {
        path: '/index/product',
        name: 'product',
        component: () => import('../views/Product/product/product.vue'),
      },
      {
        path: '/index/specification',
        name: 'specification',
        component: () =>
          import('../views/Product/specification/specification.vue'),
      },
      {
        path: '/index/announcement',
        name: 'announcement',
        component: () =>
          import('../views/Stores/announcement/announcement.vue'),
      },
      {
        path: '/index/freightTemplate',
        name: 'freightTemplate',
        component: () =>
          import('../views/Stores/freightTemplate/freightTemplate.vue'),
      },
      {
        path: '/index/HotSearch',
        name: 'HotSearch',
        component: () => import('../views/Stores/HotSearch/HotSearch.vue'),
      },
      {
        path: '/index/slideshow',
        name: 'slideshow',
        component: () => import('../views/Stores/slideshow/slideshow.vue'),
      },
      {
        path: '/index/ToThePoint',
        name: 'ToThePoint',
        component: () => import('../views/Stores/ToThePoint/ToThePoint.vue'),
      },
      {
        path: '/index/address',
        name: 'address',
        component: () => import('../views/system/address/address.vue'),
      },
      {
        path: '/index/administrator',
        name: 'administrator',
        component: () =>
          import('../views/system/administrator/administrator.vue'),
      },
      {
        path: '/index/menu',
        name: 'menu',
        component: () => import('../views/system/menu/menu.vue'),
      },
      {
        path: '/index/parameter',
        name: 'parameter',
        component: () => import('../views/system/parameter/parameter.vue'),
      },
      {
        path: '/index/role',
        name: 'role',
        component: () => import('../views/system/role/role.vue'),
      },
      {
        path: '/index/TheSystemLog',
        name: 'TheSystemLog',
        component: () =>
          import('../views/system/TheSystemLog/TheSystemLog.vue'),
      },
      {
        path: '/index/TimingTask',
        name: 'TimingTask',
        component: () => import('../views/system/TimingTask/TimingTask.vue'),
      },
      {
        path: '/index/members',
        name: 'members',
        component: () => import('../views/members/Members.vue'),
      },
      {
        path: '/index/TheOrder',
        name: 'TheOrder',
        component: () => import('../views/TheOrder/TheOrder.vue'),
      },
    ],
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/Login.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
