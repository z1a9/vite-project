import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
// 引入路由
import router from './router/index';
import App from './App.vue';

const app = createApp(App);

// 使用路由

app.use(ElementPlus).use(router).mount('#app');
